package main;

import java.util.ArrayList;
import java.util.List;

public class Ejecutable {
    public static void main(String args[]) {
        System.out.println("Programación imperativa y declarativa. ");
        List<User> users = new ArrayList<>();
        users.add(new User("Pedro", 23));
        users.add(new User("Maria", 32));
        users.add(new User("Lisa", 39));
        users.add(new User("Bart", 42));
        users.add(new User("Jose", 4));
        //Imperativo
        int contador = 0;
        for(User user: users){
            if(user.getAge() > 18){
                contador++;
            }
        }
        System.out.println("Cantidad: " + contador);
        //Declarativo
        contador = (int) users.stream().filter( user -> user.getAge() > 18 ).count();
        System.out.println("Cantidad: " + contador);
        //
        System.out.println("Fin. ");
    }
}
